import Button from '@material-ui/core/Button'
import Block from '@material-ui/icons/Block'
import { MouseEvent, useState } from 'react'
import { fetchEnd, fetchStart, useNotify, useRefresh } from 'react-admin'
import { useDispatch } from 'react-redux'

import { fetchJson } from 'src/api/dataProvider'
import envs from 'src/utils/endpoint.config'
import { Entities } from 'src/utils/enums'

import { IShortUser } from 'src/interfaces/entities/user'

const BanUserButton = ({ record }: any) => {
  const user: IShortUser = record
  const dispatch = useDispatch()
  const refresh = useRefresh()
  const notify = useNotify()
  const [loading, setLoading] = useState(false)
  const handleClick = (e: MouseEvent) => {
    e.preventDefault()
    e.stopPropagation()
    setLoading(true)
    dispatch(fetchStart()) // start the global loading indicator
    fetchJson(envs.apiUrl + `/${Entities.Users}/${user.id}/ban/`, { method: 'POST' })
      .then(() => {
        refresh()
        notify('User banned')
      })
      .catch((e) => {
        notify('Error: user not banned', { type: 'warning' })
      })
      .finally(() => {
        dispatch(fetchEnd()) // stop the global loading indicator
        setLoading(false)
      })
  }

  return (user.is_banned || user.is_deleted) ? null : (
    <Button
      variant="outlined"
      color="primary"
      size="small"
      onClick={handleClick}
      disabled={loading}
    >
      <Block color="primary" style={{ paddingRight: '0.5em', color: 'red' }} />
      Ban
    </Button>
  )
}

export default BanUserButton
