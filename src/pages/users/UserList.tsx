import { Drawer, makeStyles } from '@material-ui/core'
import { useCallback } from 'react'
import { Datagrid, DateField, ImageField, List, TextField } from 'react-admin'
import { Route, RouteChildrenProps, useHistory } from 'react-router-dom'

import UserCard from 'src/pages/users/UserCard'
import { Entities } from 'src/utils/enums'

import { IUser } from 'src/interfaces/entities/user'

import BanItemButton from 'src/components/actionButtons/BanItemButton'

const useStyles = makeStyles((theme) => ({
  drawerPaper: {
    zIndex: 100,
    paddingTop: 48,
  },
}))

const UserList = (props: any) => {
  const classes = useStyles()
  const history = useHistory()
  const onClose = useCallback(() => {
    history.push('/' + Entities.Users)
  }, [history])

  return (
    <div>
      <Route path={`/${Entities.Users}/:id`}>
        {({ match }: RouteChildrenProps<{ id: string }>) => {
          const isMatch = !!(match && match.params && match.params.id !== 'create')

          return (
            <>
              <List {...props} perPage={20} bulkActionButtons={false} exporter={false}>
                <Datagrid rowClick="edit">
                  <TextField source="name" />
                  <ImageField source="avatar" />
                  <DateField source="created_at" />
                  <BanItemButton<IUser> entity={Entities.Users} />
                </Datagrid>
              </List>
              <Drawer
                variant="persistent"
                open={isMatch}
                anchor="right"
                onClose={onClose}
                classes={{
                  paper: classes.drawerPaper,
                }}
              >
                {isMatch ? <UserCard id={(match as any).params.id} onCancel={onClose} /> : null}
              </Drawer>
            </>
          )
        }}
      </Route>
    </div>
  )
}

export default UserList
