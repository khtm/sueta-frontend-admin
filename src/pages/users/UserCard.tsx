import { Button, ImageList, ImageListItem } from '@material-ui/core'
import { isEmpty } from 'lodash'
import { FC } from 'react'
import ChatIcon from '@material-ui/icons/Chat'

import useEntityItem from 'src/hooks/useEntityItem'
import { Entities } from 'src/utils/enums'

import { IUser } from 'src/interfaces/entities/user'

import {
  ItemCard,
  classes,
  CardTextField,
  ReferenceEntityField,
} from 'src/components/itemCard/ItemCard'

const UserCard: FC<{ id: string; onCancel: () => void }> = ({ id, onCancel }) => {
  const { loading, itemData } = useEntityItem<IUser>({
    id,
    entity: Entities.Users,
  })

  return (
    <ItemCard {...{ itemData, loading, onCancel }} header="Пользователь">
      <Button
        href={`/${Entities.Chat}/?userFilter=${itemData.id}`}
        variant="outlined"
        endIcon={<ChatIcon />}
      >
        Найти чаты пользователя
      </Button>
      <div className={classes.textFieldsContainer}>
        <ReferenceEntityField image={itemData.avatar} name={itemData.name} />
        <CardTextField label="Рейтинг" value={itemData.rating} />
        <CardTextField label="Телефон" value={itemData.phone} />
        <CardTextField label="ID города" value={itemData.city_id} />
        <CardTextField label="Описание" value={itemData.description} />
        <CardTextField label="Инстаграм" value={itemData.instagram_username} />
        <CardTextField label="Кол-во подписчиков" value={itemData.subscribers_count} />
        <CardTextField label="Кол-во подписок" value={itemData.subscriptions_count} />
        <CardTextField
          label="Дата рождения"
          value={itemData.birthday}
          type="date"
          dateFormat="dd.MM.yyyy"
        />
        <CardTextField label="Регистрация" value={itemData.created_at} type="date" />
        <CardTextField label="Забанен" value={itemData.is_banned} type="bool" />
        <CardTextField label="Удален" value={itemData.is_deleted} type="bool" />
        <CardTextField label="Всего мероприятий" value={itemData.total_events_count} />
      </div>

      {!isEmpty(itemData.photos) && (
        <ImageList>
          {itemData.photos.map((v) => (
            <ImageListItem key={v}>
              <img src={v} />
            </ImageListItem>
          ))}
        </ImageList>
      )}
    </ItemCard>
  )
}

export default UserCard
