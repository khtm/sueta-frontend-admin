import { FC } from 'react'

import useEntityItem from 'src/hooks/useEntityItem'
import { Entities } from 'src/utils/enums'

import { IMark } from 'src/interfaces/entities/mark'

import { ItemCard, classes, CardTextField, ReferenceEntityField } from 'src/components/itemCard/ItemCard'

const MarkCard: FC<{ id: string; onCancel: () => void }> = ({ id, onCancel }) => {
  const { loading, itemData } = useEntityItem<IMark>({
    id,
    entity: Entities.Marks,
  })

  return (
    <ItemCard {...{ itemData, loading, onCancel }} header="Событие-метка">
      <div className={classes.textFieldsContainer}>
        <CardTextField label="Название" value={itemData.name} />
        <ReferenceEntityField
          entity={Entities.Users}
          entityId={itemData.user?.id}
          label="Организатор"
          image={itemData.user?.avatar}
          name={itemData.user?.name}
        />
        <CardTextField label="Адрес" value={itemData.address} />
        <CardTextField label="ID города" value={itemData.city_id} />
        <CardTextField label="Начало" value={itemData.start_at} type="date" />
        <CardTextField label="Окончание" value={itemData.end_at} type="date" />
        <CardTextField label="Забанено" value={itemData.is_banned} type="bool" />
        <CardTextField label="Удалено" value={itemData.is_deleted} type="bool" />
      </div>
    </ItemCard>
  )
}

export default MarkCard
