import { Drawer, makeStyles } from '@material-ui/core'
import { useCallback } from 'react'
import { Datagrid, DateField, List, TextField } from 'react-admin'
import { Route, RouteChildrenProps, useHistory } from 'react-router-dom'

import MarkCard from 'src/pages/marks/MarkCard'
import { Entities } from 'src/utils/enums'

import { IMark } from 'src/interfaces/entities/mark'

import BanItemButton from 'src/components/actionButtons/BanItemButton'
import DeleteItemButton from 'src/components/actionButtons/DeleteItemButton'

const useStyles = makeStyles((theme) => ({
  drawerPaper: {
    zIndex: 100,
    paddingTop: 48,
  },
}))

const MarkList = (props: any) => {
  const classes = useStyles()
  const history = useHistory()
  const onClose = useCallback(() => {
    history.push('/' + Entities.Marks)
  }, [history])

  return (
    <div>
      <Route path={`/${Entities.Marks}/:id`}>
        {({ match }: RouteChildrenProps<{ id: string }>) => {
          const isMatch = !!(match && match.params && match.params.id !== 'create')

          return (
            <>
              <List {...props} perPage={20} bulkActionButtons={false} exporter={false}>
                <Datagrid rowClick="edit">
                  <TextField source="name" />
                  <TextField label="User" source="user.name" />
                  <DateField source="start_at" />
                  <BanItemButton<IMark> entity={Entities.Marks} />
                  <DeleteItemButton<IMark> entity={Entities.Marks} />
                </Datagrid>
              </List>
              <Drawer
                variant="persistent"
                open={isMatch}
                anchor="right"
                onClose={onClose}
                classes={{
                  paper: classes.drawerPaper,
                }}
              >
                {isMatch ? <MarkCard id={(match as any).params.id} onCancel={onClose} /> : null}
              </Drawer>
            </>
          )
        }}
      </Route>
    </div>
  )
}

export default MarkList
