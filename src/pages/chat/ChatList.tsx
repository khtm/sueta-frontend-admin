import { ImageList, ImageListItem } from '@material-ui/core'
import TextField from '@material-ui/core/TextField'
import { compact, isEmpty, join, map, sortBy, split } from 'lodash'
import { FC, useCallback, useEffect, useMemo, useState } from 'react'
import { useNotify } from 'react-admin'
import { ChatList as Dialogs, MessageList } from 'react-chat-elements'
import qs from 'query-string'

import { fetchJson } from 'src/api/dataProvider'
import envs from 'src/utils/endpoint.config'
import { ChatRoomType, MessageType } from 'src/utils/enums'

import { IDialog, IMessage } from 'src/interfaces/entities/chat'

import classes from './ChatList.module.scss'
import { useDispatch } from 'react-redux'
import rootHistory from 'src/utils/rootHistory'

const fillEmptySlots = (imgs: string[]) => {
  if (imgs.length < 4) return imgs
  if (imgs.length % 3 === 1) return [...imgs, '']
  if (imgs.length % 3 === 2) return [...imgs, '', '']
  return imgs
}

const defaultDialogsLimit = 30
const defaultMessagesLimit = 30
const limitStep = 20

const MessageWithGallery = ({ message }: any) => {
  return (
    <div>
      <ImageList cols={Math.min(3, message.images.length)} rowHeight={200}>
        {fillEmptySlots(message.images).map((imgSrc) => (
          <ImageListItem key={imgSrc}>
            <img
              src={imgSrc}
              loading="lazy"
              onClick={() => {
                window.open(imgSrc, '_blank')
              }}
              title={imgSrc ? 'download image' : undefined}
            />
          </ImageListItem>
        ))}
      </ImageList>
      <div>{message.text}</div>
    </div>
  )
}

const ChatList: FC = () => {
  const notify = useNotify()

  const [dialogs, setDialogs] = useState<any>([])
  const [messages, setMessages] = useState<any>([])
  const [selectedDialog, setSelectedDialog] = useState<Record<string, any>>(() => {
    const query = qs.parse(location.search).dialogId as string
    return query ? { id: query } : {}
  })
  const [dialogsLimit, setDialogsLimit] = useState(defaultDialogsLimit)
  const [messagesLimit, setMessagesLimit] = useState(defaultMessagesLimit)

  // messages
  useEffect(() => {
    const { id } = selectedDialog
    if (!id) return

    fetchJson(envs.apiUrl + `/chat/${id}/message/?limit=${messagesLimit}`).then((response) => {
      const items: IMessage[] = sortBy(response.json?.items, 'created_at')
      setMessages(
        map(items, (item) => ({
          position: 'left',
          type: 'text',
          title: item.user.name,
          text: item.type === MessageType.Text ? item.text : <MessageWithGallery message={item} />,
          date: new Date(item.created_at),
          avatar: item.user.avatar,
        })),
      )
    })
  }, [selectedDialog, messagesLimit])

  const [userFilter, _setUserFilter] = useState<string>(() => {
    const query = qs.parse(location.search).userFilter as string
    return query || ''
  })
  const setUserFilter = useCallback(
    (value: string) => {
      setDialogsLimit(defaultDialogsLimit)
      _setUserFilter(value)
      rootHistory.replace({
        search: qs.stringify({ ...qs.parse(location.search), userFilter: value || undefined }),
      })
    },
    [_setUserFilter],
  )

  const chatQueries = useMemo<string>(() => {
    const userIds = compact(split(userFilter.replace(/\s+/g, ''), ','))
    const chatQueries =
      `?limit=${dialogsLimit}&` +
      join(
        map(userIds, (id) => 'user_ids[]=' + id),
        '&',
      )
    return chatQueries
  }, [userFilter, dialogsLimit])

  // dialogs
  useEffect(() => {
    fetchJson(envs.apiUrl + '/chat/' + chatQueries)
      .then((response) => {
        const items: IDialog[] = response.json.items
        setDialogs(
          map(items, (item) => ({
            avatar:
              item.general_chat?.image ||
              item.event?.image ||
              'data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=',
            alt: '',
            title:
              item.general_chat?.name ||
              item.event?.name ||
              `[${join(map(item.users, 'name'), ', ')}]`,
            subtitle:
              (item.is_blocked ? '[blocked] ' : '') + ChatRoomType[item.type] + ' chat room',
            date: new Date(item.updated_at),
            unread: 0,
            id: item.id,
          })),
        )
      })
      .catch(() => {
        notify('Error on fetching chat list', 'warning')
      })
  }, [chatQueries])

  return (
    <>
      <TextField
        id="outlined-basic"
        label="Filter by users (id-1, id-2, id-3)"
        variant="outlined"
        size="small"
        onBlur={({ target }) => {
          setUserFilter(target.value)
        }}
        className={classes.userFilter}
        defaultValue={userFilter}
      />
      <div className={classes.root}>
        <div className={classes.dialogs}>
          <Dialogs
            dataSource={dialogs}
            onClick={(value: any) => {
              setMessagesLimit(defaultMessagesLimit)
              setSelectedDialog(value)
              rootHistory.replace({
                search: qs.stringify({ ...qs.parse(location.search), dialogId: value.id }),
              })
            }}
          />
          {Boolean(dialogs.length) && (
            <div
              className={classes.loadMore}
              onClick={() => {
                setDialogsLimit(dialogsLimit + limitStep)
              }}
            >
              Load more
            </div>
          )}
        </div>
        <div className={classes.messages}>
          {Boolean(messages.length) && (
            <div
              className={classes.loadMore}
              onClick={() => {
                setMessagesLimit(messagesLimit + limitStep)
              }}
            >
              Load more
            </div>
          )}
          {Boolean(selectedDialog?.id) && isEmpty(messages) ? (
            <span className={classes.emptyDialog}>chat is empty</span>
          ) : (
            <MessageList dataSource={messages} />
          )}
        </div>
      </div>
    </>
  )
}

export default ChatList
