import { Button } from '@material-ui/core'
import ChatIcon from '@material-ui/icons/Chat'
import { formatDuration } from 'date-fns'
import ru from 'date-fns/locale/ru'
import { isEmpty, join, map } from 'lodash'
import { FC } from 'react'

import useEntityItem from 'src/hooks/useEntityItem'
import { Entities, EventCategory, EventPeriodicityType, EventScopeVisible } from 'src/utils/enums'

import { IEvent } from 'src/interfaces/entities/event'

import {
  CardTextField,
  classes,
  ItemCard,
  ReferenceEntityField,
} from 'src/components/itemCard/ItemCard'

const EventCard: FC<{ id: string; onCancel: () => void }> = ({ id, onCancel }) => {
  const { loading, itemData } = useEntityItem<IEvent>({ id, entity: Entities.Events })

  return (
    <ItemCard {...{ itemData, loading, onCancel }} header="Информация о событии">
      <Button
        href={`/${Entities.Chat}/?dialogId=${itemData.id}`}
        variant="outlined"
        endIcon={<ChatIcon />}
      >
        Перейти в чат
      </Button>
      {Boolean(itemData.image) && (
        <img src={itemData.image} className={classes.image} height={300} width="auto" />
      )}
      <div className={classes.textFieldsContainer}>
        <ReferenceEntityField
          entity={Entities.Users}
          entityId={itemData.user?.id}
          label="Организатор"
          image={itemData.user?.avatar}
          name={itemData.user?.name}
        />
        <CardTextField label="Рейтинг организатора" value={itemData.user?.rating} />
        <CardTextField label="Название" value={itemData.name} />
        <CardTextField label="Категория" value={EventCategory[itemData.category]} />
        <CardTextField label="Город" value={itemData.city?.name} />
        <CardTextField label="Адрес" value={itemData.address} />
        <CardTextField label="Участников" value={itemData.active_members_count} />
        <CardTextField label="Начало" value={itemData.start_at} type="date" />
        <CardTextField
          label="Длительность"
          value={formatDuration(
            {
              days: Math.floor(itemData.duration / 24),
              hours: itemData.duration % 24,
            },
            {
              locale: ru,
            },
          )}
        />
        <CardTextField label="Цена" value={itemData.price} type="currency" />
        <CardTextField label="Описание" value={itemData.description} />
        <CardTextField label="Видимость для" value={EventScopeVisible[itemData.scope_visible]} />
        <CardTextField label="Забанено" value={itemData.is_banned} type="bool" />
        <CardTextField label="Удалено" value={itemData.is_deleted} type="bool" />
        <CardTextField label="Создано" value={itemData.created_at} type="date" />
        <CardTextField label="Обновлено" value={itemData.updated_at} type="date" />
        <CardTextField label="Скрыть адрес" value={itemData.is_hide_address} type="bool" />
        <CardTextField label="Скрыть альбом" value={itemData.is_hide_album} type="bool" />
        <CardTextField label="Скрыть участников" value={itemData.is_hide_members} type="bool" />
        <CardTextField
          label="Запрос для вступления"
          value={itemData.is_request_membership_access}
          type="bool"
        />
        <CardTextField
          label="Периодичность"
          value={EventPeriodicityType[itemData.periodicity_type]}
        />
        {/* 
            TODO: the feature is deactivated RN
            {
              itemData.periodicity_type === EventPeriodicityType.Repeatable && (
                // periodicity_weekdays ?
              )
            } */}
      </div>
      {!isEmpty(itemData.tag_list) && (
        <CardTextField
          label="Теги"
          value={join(
            map(itemData.tag_list, (v) => '#' + v),
            ' ',
          )}
        />
      )}
    </ItemCard>
  )
}

export default EventCard
