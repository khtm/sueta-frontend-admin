import { Drawer, makeStyles } from '@material-ui/core'
import { includes } from 'lodash'
import { useCallback } from 'react'
import { Datagrid, List, TextField } from 'react-admin'
import { Route, RouteChildrenProps, useHistory } from 'react-router-dom'

import EventCard from 'src/pages/events/EventCard'
import { Entities } from 'src/utils/enums'

import { IEvent } from 'src/interfaces/entities/event'

import BanItemButton from 'src/components/actionButtons/BanItemButton'
import DeleteItemButton from 'src/components/actionButtons/DeleteItemButton'

const useStyles = makeStyles((theme) => ({
  drawerPaper: {
    zIndex: 100,
    paddingTop: 48,
  },
}))

const EventList = (props: any) => {
  const classes = useStyles()
  const history = useHistory()
  const onClose = useCallback(() => {
    history.push('/' + Entities.Events)
  }, [history])
  return (
    <div>
      <Route path={`/${Entities.Events}/:id`}>
        {({ match }: RouteChildrenProps<{ id: string }>) => {
          const isMatch = !!(
            match &&
            match.params &&
            !includes(['create', 'report'], match.params.id)
          )

          return (
            <>
              <List {...props} perPage={20} bulkActionButtons={false} exporter={false}>
                <Datagrid rowClick="edit">
                  <TextField source="name" />
                  <TextField label="User name" source="user.name" />
                  <TextField source="address" />
                  <BanItemButton<IEvent> entity={Entities.Events} />
                  <DeleteItemButton<IEvent> entity={Entities.Events} />
                </Datagrid>
              </List>
              <Drawer
                variant="persistent"
                open={isMatch}
                anchor="right"
                onClose={onClose}
                classes={{
                  paper: classes.drawerPaper,
                }}
              >
                {/* To avoid any errors if the route does not match, we don't render at all the component in this case */}
                {isMatch ? (
                  <EventCard
                    id={(match as any).params.id}
                    onCancel={onClose}
                    // resource={props.resource}
                    // basePath={props.basePath}
                  />
                ) : null}
              </Drawer>
            </>
          )
        }}
      </Route>
    </div>
  )
}

export default EventList
