import { FC } from 'react'

import useEntityItem from 'src/hooks/useEntityItem'
import { Entities, ReportType } from 'src/utils/enums'

import { IEventReport } from 'src/interfaces/entities/eventReport'

import {
  ItemCard,
  classes,
  CardTextField,
  ReferenceEntityField,
} from 'src/components/itemCard/ItemCard'

const ReportCard: FC<{ id: string; onCancel: () => void }> = ({ id, onCancel }) => {
  const { loading, itemData } = useEntityItem<IEventReport>({
    id,
    entity: Entities.EventReports,
  })

  return (
    <ItemCard {...{ itemData, loading, onCancel }} header="Жалоба на событие">
      <div className={classes.textFieldsContainer}>
        <ReferenceEntityField
          entity={Entities.Events}
          entityId={itemData.event?.id}
          label="Событие"
          image={itemData.event?.image}
          name={itemData.event?.name}
        />
        <ReferenceEntityField
          entity={Entities.Users}
          entityId={itemData.complainer?.id}
          label="Обратившийся"
          image={itemData.complainer?.avatar}
          name={itemData.complainer?.name}
        />
        <CardTextField label="Причина" value={ReportType[itemData.type]} />
        <CardTextField label="Подробности" value={itemData.text} />
        <CardTextField label="Дата" value={itemData.created_at} type="date" />
      </div>
    </ItemCard>
  )
}

export default ReportCard
