import { Drawer, makeStyles } from '@material-ui/core'
import { useCallback } from 'react'
import { Datagrid, DateField, ImageField, List, TextField } from 'react-admin'
import { Route, RouteChildrenProps, useHistory } from 'react-router-dom'

import { Entities } from 'src/utils/enums'

import ReportCard from 'src/pages/userReports/UserReportCard'

const useStyles = makeStyles((theme) => ({
  drawerPaper: {
    zIndex: 100,
    paddingTop: 48,
  },
}))

const ReportList = (props: any) => {
  const classes = useStyles()
  const history = useHistory()
  const onClose = useCallback(() => {
    history.push('/' + Entities.UserReports)
  }, [history])

  return (
    <div>
      <Route path={`/${Entities.UserReports}/:id`}>
        {({ match }: RouteChildrenProps<{ id: string }>) => {
          const isMatch = !!(match && match.params && match.params.id !== 'create')

          return (
            <>
              <List {...props} perPage={20} bulkActionButtons={false} exporter={false}>
                <Datagrid rowClick="edit">
                  <TextField label="User name" source="user.name" />
                  <ImageField label="User image" source="user.image" />
                  <DateField source="created_at" />
                </Datagrid>
              </List>
              <Drawer
                variant="persistent"
                open={isMatch}
                anchor="right"
                onClose={onClose}
                classes={{
                  paper: classes.drawerPaper,
                }}
              >
                {isMatch ? (
                  <ReportCard
                    id={(match as any).params.id}
                    onCancel={onClose}
                  />
                ) : null}
              </Drawer>
            </>
          )
        }}
      </Route>
    </div>
  )
}

export default ReportList
