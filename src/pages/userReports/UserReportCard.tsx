import { FC } from 'react'

import useEntityItem from 'src/hooks/useEntityItem'
import { Entities, ReportType } from 'src/utils/enums'

import { IUserReport } from 'src/interfaces/entities/userReport'

import {
  ItemCard,
  classes,
  CardTextField,
  ReferenceEntityField,
} from 'src/components/itemCard/ItemCard'

const ReportCard: FC<{ id: string; onCancel: () => void }> = ({ id, onCancel }) => {
  const { loading, itemData } = useEntityItem<IUserReport>({
    id,
    entity: Entities.UserReports,
  })

  return (
    <ItemCard {...{ itemData, loading, onCancel }} header="Жалоба на юзера">
      <div className={classes.textFieldsContainer}>
        <ReferenceEntityField
          entity={Entities.Users}
          entityId={itemData.complainer?.id}
          label="Обратившийся"
          image={itemData.complainer?.avatar}
          name={itemData.complainer?.name}
        />
        <ReferenceEntityField
          entity={Entities.Users}
          entityId={itemData.user?.id}
          label="Обвиняемый"
          image={itemData.user?.avatar}
          name={itemData.user?.name}
        />
        <CardTextField label="Причина" value={ReportType[itemData.type]} />
        <CardTextField label="Подробности" value={itemData.text} />
        <CardTextField label="Дата" value={itemData.created_at} type="date" />
      </div>
    </ItemCard>
  )
}

export default ReportCard
