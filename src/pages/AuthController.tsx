import LinearProgress from '@material-ui/core/LinearProgress'
import differenceInMinutes from 'date-fns/differenceInMinutes'
import { get } from 'lodash'
import { FC, useEffect, useState } from 'react'

import authProvider from 'src/api/auth'
import { fetchJson } from 'src/api/dataProvider'
import envs from 'src/utils/endpoint.config'
import { LSProps } from 'src/utils/enums'
import rootHistory from 'src/utils/rootHistory'

import { ITokenInfo } from 'src/interfaces/auth'

const AuthController: FC = ({ children }) => {
  const [permissionGranted, grantPermission] = useState(false)

  useEffect(() => {
    const access: ITokenInfo = JSON.parse(localStorage.getItem(LSProps.AccessToken) || '{}')

    !access?.token && grantPermission(true)

    if (differenceInMinutes(new Date(access.exp), new Date()) < 120) {
      fetchJson(envs.apiUrl + '/auth/token/refresh/', {
        method: 'POST',
        body: localStorage.getItem(LSProps.RefreshToken),
      })
        .then((response) => {
          const accessToken: object = get(response, 'json.access_token')
          if (!accessToken) {
            authProvider.logout({})
            rootHistory.push({
              pathname: '/login',
              state: { nextPathname: window.location.pathname, nextSearch: window.location.search },
            })
          } else {
            const refreshToken: object = get(response, 'json.refresh_token')
            localStorage.setItem(LSProps.AccessToken, JSON.stringify(accessToken))
            localStorage.setItem(LSProps.RefreshToken, JSON.stringify(refreshToken))
          }
        })
        .finally(() => {
          grantPermission(true)
        })
    } else {
      grantPermission(true)
    }
  }, [])

  return permissionGranted ? <>{children}</> : <LinearProgress />
}

export default AuthController
