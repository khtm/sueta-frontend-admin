export interface ITokenInfo {
  token: string
  exp: string
}
