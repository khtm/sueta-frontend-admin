export interface IShortMark {
  id: string
  user: {
    id: string
    name: string
    avatar: string
  }
  created_at: string
  city_id: string
  is_deleted: boolean
  is_banned: boolean
  name: string
  start_at: string
}

export interface IMark extends IShortMark {
  location: {
    lon: number
    lat: number
  }
  address: string
  end_at: string
}
