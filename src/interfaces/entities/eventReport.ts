import { ReportType } from 'src/utils/enums'

export interface IShortEventReport {
  id: string
  type: ReportType
  event: {
    id: string
    name: string
    image: string
  }
  created_at: string
}

export interface IEventReport extends IShortEventReport {
  complainer: {
    id: string
    name: string
    avatar: string
  }
  text: string
}
