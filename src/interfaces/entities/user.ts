export interface IShortUser {
  id: string
  name: string
  avatar: string
  is_deleted: boolean
  is_banned: boolean
  created_at: string
}

export interface IUser extends IShortUser {
  auth_provider_type: number
  auth_provider_id: string
  phone: string
  birthday: string
  city_id: string
  photos: string[]
  rating: number
  description: string
  instagram_username: string
  total_events_count: number
  subscribers_count: number
  subscriptions_count: number
  updated_at: string
}