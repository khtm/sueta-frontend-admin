import { ReportType } from 'src/utils/enums'

export interface IShortUserReport {
  id: string
  type: ReportType
  user: {
    id: string
    name: string
    avatar: string
  }
  created_at: string
}

export interface IUserReport extends IShortUserReport {
  complainer: {
    id: string
    name: string
    avatar: string
  }
  text: string
}
