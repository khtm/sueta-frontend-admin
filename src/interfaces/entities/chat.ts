import { ChatRoomType, EventCategory, MessageType } from 'src/utils/enums'

export interface IDialog {
  id: string
  type: ChatRoomType
  created_at: string
  updated_at: string
  is_blocked: boolean
  event_id: string
  general_chat?: {
    image: string
    name: string
  }
  event?: {
    id: number
    user_id: string
    name: string
    category: EventCategory
    image: string
  }
  users: {
    id: string
    name: string
    avatar: string
  }[]
}

export interface IMessage {
  id: string
  type: MessageType
  text: string
  is_deleted: boolean
  created_at: string
  images: string[]
  user: {
    id: string
    name: string
    avatar: string
  }
}
