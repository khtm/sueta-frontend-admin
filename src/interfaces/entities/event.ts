import { EventCategory, EventPeriodicityType, EventScopeVisible } from 'src/utils/enums'

export interface IShortEvent {
  id: number
  category: EventCategory
  name: string
  created_at: string
  start_at: string
  end_at: string
  duration: number
  address: string
  image: string
  user: {
    id: string
    name: string
    rating: number
    avatar: string
  }
  is_banned: boolean
  is_deleted: boolean
}

export interface IEvent extends IShortEvent {
  city: {
    id: number
    name: string
  }
  periodicity_type: EventPeriodicityType
  periodicity_from_at: string
  periodicity_to_at: string
  periodicity_weekdays: (string | null)[]
  scope_visible: EventScopeVisible
  is_hide_address: boolean
  is_hide_members: boolean
  is_hide_album: boolean
  is_request_membership_access: boolean
  tag_list: string[]
  active_members_count: number
  updated_at: string
  price: number
  description: string
}
