import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

import './index.css'
import 'react-chat-elements/dist/main.css'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

/**
 * TODO list
 * 
 * - refresh token system (https://marmelab.com/blog/2020/07/02/manage-your-jwt-react-admin-authentication-in-memory.html)
 */
