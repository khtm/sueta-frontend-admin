import { createBrowserHistory } from 'history'

const rootHistory = createBrowserHistory()

export default rootHistory
