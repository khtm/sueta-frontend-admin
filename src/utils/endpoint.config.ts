import { get } from 'lodash'

const safeGetString = (key: string) => get(process.env, key, '')

const envs = {
  apiUrl: safeGetString('REACT_APP_API_URL'),
  adminPassword: safeGetString('REACT_APP_ADMIN_PASSWORD'),
}

export default envs
