export enum LSProps {
  AccessToken = 'accessToken',
  RefreshToken = 'refreshToken',
}

export enum Entities {
  Users = 'user',
  Events = 'event',
  Chat = 'chat',
  EventReports = 'event/report',
  UserReports = 'user/report',
  Marks = 'mark',
}

export enum EventCategory {
  Sport = 1,
  Entertainment,
  Party,
  Help,
  Stroll,
  Tourism,
  Hiking,
  Art,
  Training,
  Concert,
  BoardGames,
  Gastronomy,
}

export enum EventPeriodicityType {
  Uniq = 1,
  Repeatable,
}

export enum EventMemberStatus {
  Owner = 1,
  Requested,
  Approved,
  Invited,
  Kicked,
}

export enum EventScopeVisible {
  All = 1,
  Subscriptions,
  Nobody,
}

export enum ReportType {
  Spam = 1,
  Violence,
  Pornography,
  ChildAbuse,
  Other,
}

export enum ChatRoomType {
  Private = 1,
  Event,
  General,
}

export enum MessageType {
  Text = 1,
  Image,
}
