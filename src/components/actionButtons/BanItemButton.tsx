import Button from '@material-ui/core/Button'
import Block from '@material-ui/icons/Block'
import { MouseEvent, useState } from 'react'
import { fetchEnd, fetchStart, useNotify, useRefresh } from 'react-admin'
import { useDispatch } from 'react-redux'

import { fetchJson } from 'src/api/dataProvider'
import envs from 'src/utils/endpoint.config'
import { Entities } from 'src/utils/enums'

interface IBaseRecord {
  id: string | number
  is_deleted?: boolean
  is_banned?: boolean
}

interface IProps<T> {
  record?: T
  entity: Entities
  entityLabel?: string
}

const BanItemButton = <T extends IBaseRecord>({
  record,
  entity,
  entityLabel = entity,
}: IProps<T>) => {
  const dispatch = useDispatch()
  const refresh = useRefresh()
  const notify = useNotify()
  const [loading, setLoading] = useState(false)
  const handleClick = (e: MouseEvent) => {
    e.preventDefault()
    e.stopPropagation()
    setLoading(true)
    dispatch(fetchStart()) // start the global loading indicator
    fetchJson(envs.apiUrl + `/${entity}/${record!.id}/ban/`, { method: 'POST' })
      .then(() => {
        refresh()
        notify(`${entityLabel} banned`)
      })
      .catch((e) => {
        notify(`Error: ${entityLabel} not banned`, { type: 'warning' })
      })
      .finally(() => {
        dispatch(fetchEnd()) // stop the global loading indicator
        setLoading(false)
      })
  }

  return record!.is_banned || record!.is_deleted ? null : (
    <Button
      variant="outlined"
      color="primary"
      size="small"
      onClick={handleClick}
      disabled={loading}
    >
      <Block color="primary" style={{ paddingRight: '0.5em', color: 'red' }} />
      Ban
    </Button>
  )
}

export default BanItemButton
