import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Avatar,
  CircularProgress,
  IconButton,
  Typography,
} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import classNames from 'classnames'
import { format } from 'date-fns'
import { isEmpty, isNil } from 'lodash'
import { FC, Fragment, useMemo } from 'react'
import { Link } from 'react-router-dom'
import { Entities } from 'src/utils/enums'

import classes from './ItemCard.module.scss'

const CardTextField = ({
  label,
  value = '–',
  type,
  dateFormat = 'dd.MM.yy HH:mm',
}: {
  label: string
  value: string | number | boolean
  type?: 'date' | 'currency' | 'bool'
  dateFormat?: string
}) => {
  const formattedValue = useMemo(() => {
    switch (type) {
      case 'date':
        return format(new Date(value as string), dateFormat)
      case 'currency':
        return `${value}₽`
      case 'bool':
        return value ? 'Да' : 'Нет'
      default:
        return value
    }
  }, [value])
  return !isNil(value) ? (
    <div className={classes.textField}>
      <div className={classes.textFieldLabel}>{label}</div>
      <div className={classes.textFieldValue}>{formattedValue}</div>
    </div>
  ) : null
}

const ReferenceEntityField = ({
  label,
  image,
  name,
  entity,
  entityId
}: {
  label?: string
  image: string
  name: string
  entity?: Entities
  entityId?: string
}) => {
  const Root = entity && entityId ? Link : Fragment
  return (
    <Root to={`/${entity}/${entityId}`}>
      <div className={classes.textField}>
        <div className={classes.textFieldLabel}>{label}</div>
        <div
          className={classNames(classes.referenceField, { [classes.bigReferenceField]: !label })}
        >
          <Avatar src={image} imgProps={{ height: 25, width: 25 }} />
          <span className={classes.textFieldValue}>{name}</span>
        </div>
      </div>
    </Root>
  )
}

interface IItemCardProps {
  onCancel: () => void
  header: string
  loading: boolean
  itemData: object
}

const ItemCard: FC<IItemCardProps> = ({ onCancel, header, loading, itemData, children }) => {
  return (
    <div className={classes.root}>
      <div className={classes.title}>
        <Typography variant="h6">{header}</Typography>
        <IconButton onClick={onCancel}>
          <CloseIcon />
        </IconButton>
      </div>
      {loading || isEmpty(itemData) ? (
        <CircularProgress />
      ) : (
        <>
          {children}

          <Accordion>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Typography>Show JSON</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <pre style={{ fontSize: 12 }}>{JSON.stringify(itemData, undefined, 2)}</pre>
            </AccordionDetails>
          </Accordion>
        </>
      )}
    </div>
  )
}

export { ItemCard, CardTextField, ReferenceEntityField, classes }
