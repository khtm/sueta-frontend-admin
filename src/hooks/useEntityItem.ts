import { useEffect, useState } from 'react'

import { fetchJson } from 'src/api/dataProvider'
import envs from 'src/utils/endpoint.config'
import { Entities } from 'src/utils/enums'

interface IProps {
  id: string | number
  entity: Entities
}

const useEntityItem = <T>({ id, entity }: IProps) => {
  const [loading, setLoading] = useState(false)
  const [itemData, setItemData] = useState<T>({} as T)
  useEffect(() => {
    setLoading(true)
    fetchJson(envs.apiUrl + `/${entity}/${id}/`)
      .then((response) => {
        setItemData(response.json)
      })
      .finally(() => {
        setLoading(false)
      })
  }, [id])
  return { loading, itemData }
}

export default useEntityItem
