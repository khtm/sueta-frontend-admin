import { stringify } from 'query-string'
import { DataProvider, fetchUtils } from 'ra-core'

import authProvider from 'src/api/auth'
import envs from 'src/utils/endpoint.config'
import { LSProps } from 'src/utils/enums'
import rootHistory from 'src/utils/rootHistory'

import tokenValidityWatcher from 'src/api/tokenValidityWatcher'

export const fetchJson = async (url: string, options: any = {}) => {
  await tokenValidityWatcher()
  try {
    if (!options.headers) {
      options.headers = new Headers({ Accept: 'application/json' })
    }
    const { token } = JSON.parse(localStorage.getItem(LSProps.AccessToken) || '')
    token && options.headers.set('Authorization', 'Bearer ' + token)
  } catch (e) {
    console.log(e)
  }
  return fetchUtils.fetchJson(url, options).catch((props) => {
    if (props.status === 401) {
      authProvider.logout({})
      rootHistory.push({
        pathname: '/login',
        state: { nextPathname: window.location.pathname, nextSearch: window.location.search },
      })
    }
    return props
  })
}

const dataProvider = (apiUrl = envs.apiUrl, httpClient = fetchJson): DataProvider => ({
  getList: (resource, params) => {
    const { page, perPage } = params.pagination
    const { field, order } = params.sort
    const query = {
      ...fetchUtils.flattenObject(params.filter),
      limit: perPage,
      offset: (page - 1) * perPage,
    }
    const url = `${apiUrl}/${resource}/?${stringify(query)}`

    return httpClient(url).then(({ headers, json }) => {
      return {
        data: json.items,
        total: json.total,
      }
    })
  },

  getOne: (resource, params) =>
    httpClient(`${apiUrl}/${resource}/${params.id}`).then(({ json }) => ({
      data: json,
    })),

  getMany: (resource, params) => {
    const query = {
      id: params.ids,
    }
    const url = `${apiUrl}/${resource}?${stringify(query)}`
    return httpClient(url).then(({ json }) => ({ data: json }))
  },

  getManyReference: (resource, params) => {
    const { page, perPage } = params.pagination
    const { field, order } = params.sort
    const query = {
      ...fetchUtils.flattenObject(params.filter),
      [params.target]: params.id,
      _sort: field,
      _order: order,
      _start: (page - 1) * perPage,
      _end: page * perPage,
    }
    const url = `${apiUrl}/${resource}?${stringify(query)}`

    return httpClient(url).then(({ headers, json }) => {
      return {
        data: json.items,
        total: json.total,
      }
    })
  },

  update: (resource, params) =>
    httpClient(`${apiUrl}/${resource}/${params.id}`, {
      method: 'PUT',
      body: JSON.stringify(params.data),
    }).then(({ json }) => ({ data: json })),

  // json-server doesn't handle filters on UPDATE route, so we fallback to calling UPDATE n times instead
  updateMany: (resource, params) =>
    Promise.all(
      params.ids.map((id) =>
        httpClient(`${apiUrl}/${resource}/${id}`, {
          method: 'PUT',
          body: JSON.stringify(params.data),
        }),
      ),
    ).then((responses) => ({ data: responses.map(({ json }) => json.id) })),

  create: (resource, params) =>
    httpClient(`${apiUrl}/${resource}`, {
      method: 'POST',
      body: JSON.stringify(params.data),
    }).then(({ json }) => ({
      data: { ...params.data, id: json.id },
    })),

  delete: (resource, params) => {
    return httpClient(`${apiUrl}/${resource}/${params.id}/`, {
      method: 'DELETE',
    }).then(({ json }) => ({ data: json }))
  },

  // json-server doesn't handle filters on DELETE route, so we fallback to calling DELETE n times instead
  deleteMany: (resource, params) =>
    Promise.all(
      params.ids.map((id) =>
        httpClient(`${apiUrl}/${resource}/${id}`, {
          method: 'DELETE',
        }),
      ),
    ).then((responses) => ({ data: responses.map(({ json }) => json.id) })),
})

// const dataProvider = jsonServerProvider('https://jsonplaceholder.typicode.com', fetchJson);

export default dataProvider
