import axios from 'axios'
import { get } from 'lodash'

import envs from 'src/utils/endpoint.config'
import { LSProps } from 'src/utils/enums'

import { ITokenInfo } from 'src/interfaces/auth'

const promiseState = (p: Promise<any>) => {
  const t = {}
  return Promise.race([p, t]).then(
    (v) => (v === t ? 'pending' : 'fulfilled'),
    () => 'rejected',
  )
}

const refreshApi = () => {
  return axios
    .post(
      envs.apiUrl + '/auth/token/refresh/',
      { token: JSON.parse(localStorage.getItem(LSProps.RefreshToken) || '{}').token },
      {
        headers: {
          Authorization: JSON.parse(localStorage.getItem(LSProps.AccessToken) || '{}').token,
        },
      },
    )
    .then((response) => {
      const accessToken: object = get(response, 'data.access_token')
      const refreshToken: object = get(response, 'data.refresh_token')
      localStorage.setItem(LSProps.AccessToken, JSON.stringify(accessToken))
      localStorage.setItem(LSProps.RefreshToken, JSON.stringify(refreshToken))
      return { accessToken, refreshToken }
    })
}

let refreshPromise: ReturnType<typeof refreshApi> = new Promise((resolve) => {
  resolve(null as any)
})

const shouldWeRefresh = () => {
  const access: ITokenInfo = JSON.parse(localStorage.getItem(LSProps.AccessToken) || '{}')
  const refresh: ITokenInfo = JSON.parse(localStorage.getItem(LSProps.RefreshToken) || '{}')
  const currentDate = new Date()

  return new Date(access.exp) < currentDate && new Date(refresh.exp) > currentDate
}

const tokenValidityWatcher = () =>
  new Promise((resolve) => {
    if (shouldWeRefresh()) {
      promiseState(refreshPromise).then((state) => {
        if (state === 'pending') {
          refreshPromise.then(() => {
            resolve(null)
          })
        } else {
          refreshPromise = refreshApi()
          refreshPromise.then(() => {
            resolve(null)
          })
        }
      })
    } else {
      resolve(null)
    }
  })

export default tokenValidityWatcher
