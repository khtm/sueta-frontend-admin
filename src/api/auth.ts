import axios from 'axios'
import { get } from 'lodash'
import { AuthProvider } from 'react-admin'

import envs from 'src/utils/endpoint.config'
import { LSProps } from 'src/utils/enums'

const onLogout = () => {
  localStorage.removeItem(LSProps.AccessToken);
  localStorage.removeItem(LSProps.RefreshToken);
}

const authProvider: AuthProvider = {
    login: async ({ username, password }) => {
      const response = await axios.post(envs.apiUrl + '/auth/login/', { password: envs.adminPassword })
      const accessToken: object = get(response, 'data.access_token')
      if (!accessToken) return Promise.reject('Please check your password')
      const refreshToken: object = get(response, 'data.refresh_token')
      localStorage.setItem(LSProps.AccessToken, JSON.stringify(accessToken));
      localStorage.setItem(LSProps.RefreshToken, JSON.stringify(refreshToken));
      return Promise.resolve();
    },
    logout: () => {
      onLogout()
      return Promise.resolve();
    },
    checkError: (error) => {
      const status = error.status;
      if (status === 401 || status === 403) {
          onLogout()
          return Promise.reject();
      }
      // other error code (404, 500, etc): no need to log out
      return Promise.resolve();
    },
    checkAuth: () => {
      return localStorage.getItem(LSProps.AccessToken) ? Promise.resolve() : Promise.reject()
    },
    getPermissions: () => Promise.reject('Unknown method'),
    getIdentity: () =>
        Promise.resolve({
            id: 'user',
            fullName: 'Администратор суеты',
        }),
};

export default authProvider;