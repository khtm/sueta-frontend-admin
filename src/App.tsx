import ChatIcon from '@material-ui/icons/Chat'
import UserIcon from '@material-ui/icons/Group'
import PersonPinCircleIcon from '@material-ui/icons/PersonPinCircle'
import ReportIcon from '@material-ui/icons/Report'
import SportsEsportsIcon from '@material-ui/icons/SportsEsports'
import { Admin, Resource } from 'react-admin'

import authProvider from 'src/api/auth'
import dataProvider from 'src/api/dataProvider'
import { Entities } from 'src/utils/enums'
import rootHistory from 'src/utils/rootHistory'

import AuthController from 'src/pages/AuthController'
import ChatList from 'src/pages/chat/ChatList'
import Dashboard from 'src/pages/Dashboard'
import EventReportList from 'src/pages/eventReports/EventReportList'
import EventList from 'src/pages/events/EventList'
import MarkList from 'src/pages/marks/MarkList'
import UserReportList from 'src/pages/userReports/UserReportList'
import UserList from 'src/pages/users/UserList'

const App = () => (
  // <AuthController>
  <Admin
    dataProvider={dataProvider()}
    authProvider={authProvider}
    dashboard={Dashboard}
    history={rootHistory}
    disableTelemetry
  >
    <Resource
      name={Entities.EventReports}
      icon={ReportIcon}
      list={EventReportList}
      options={{ label: 'Event reports' }}
    />
    <Resource
      name={Entities.UserReports}
      icon={ReportIcon}
      list={UserReportList}
      options={{ label: 'User reports' }}
    />
    <Resource name={Entities.Events} icon={SportsEsportsIcon} list={EventList} />
    <Resource name={Entities.Users} icon={UserIcon} list={UserList} />
    <Resource name={Entities.Chat} icon={ChatIcon} list={ChatList} />
    <Resource name={Entities.Marks} icon={PersonPinCircleIcon} list={MarkList} />
  </Admin>
  // </AuthController>
)

export default App
